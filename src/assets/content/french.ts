import { type Resume, type Website, resumePartial, techs } from './index'

import DNDMapIcon from '../../components/icons/DNDMap.vue'
import KenneyJamIcon from '../../components/icons/KenneyJam.vue'
import SkillcraftExplorerIcon from '../../components/icons/SkillcraftExplorer.vue'
import DataEntryIcon from '@/components/icons/DataEntry.vue'
import EForumIcon from '../../components/icons/EForum.vue'
import ETimesheetIcon from '../../components/icons/ETimesheet.vue'
import CyberJayIcon from '../../components/icons/CyberJay.vue'
import BinarPortfolioIcon from '../../components/icons/BinarPortfolio.vue'
import MinecraftRepoIcon from '../../components/icons/MinecraftRepo.vue'
import LootReportIcon from '../../components/icons/LootReport.vue'
import LightCompanionIcon from '../../components/icons/LightCompanion.vue'
import TheRogueMerchantIcon from '../../components/icons/TheRogueMerchant.vue'
import MagicMouthIcon from '../../components/icons/MagicMouth.vue'
import CrimsonReportIcon from '../../components/icons/CrimsonReport.vue'
import MegaRobotsAPIIcon from '../../components/icons/MegaRobotsAPI.vue'
import DestinySeasonPredictionsIcon from '../../components/icons/DestinySeasonPredictions.vue'
import WastedOnDestinyIcon from '../../components/icons/WastedOnDestiny.vue'
import OublietteXYZIcon from '../../components/icons/OublietteXYZ.vue'
import WebGallery2Icon from '../../components/icons/WebGallery2.vue'
import WebGallery1Icon from '../../components/icons/WebGallery1.vue'
import ExaltedManagerIcon from '../../components/icons/ExaltedManager.vue'
import RollForInitiativeIcon from '../../components/icons/RollForInitiative.vue'
import ZeldaGearIcon from '../../components/icons/ZeldaGear.vue'
import SayonaraMemoriesIcon from '../../components/icons/SayonaraMemories.vue'
import TinyTemplateIcon from '../../components/icons/TinyTemplate.vue'
import APIneIcon from '../../components/icons/APIne.vue'
import SmallNameIcon from '../../components/icons/SmallName.vue'
import DinklebotNetIcon from '../../components/icons/DinklebotNet.vue'
import WarOfRaekidionIcon from '../../components/icons/WarOfRaekidion.vue'
import NESBlobIcon from '../../components/icons/NESBlob.vue'

export const resume: Resume = {
  ...resumePartial,
  ...{
    name: 'François Allard',
    summary: 'Je suis un développeur web passionné par l\'innovation et les nouvelles technologies. Je n\'hésite pas à montrer les embuches possibles et à chercher des solutions avant qu\'elles ne deviennent un problème. Je crois qu\'apprendre et partager nos connaissances est ce qui nous fait le plus grandir. Je suis une personne autocritique, autodidacte et persévérante.',
    achievements: [
      'J\'ai fait passer des applications difficiles à maintenir en services plus petits et plus faciles à gérer',
      'J\'ai aidé mes pairs à mieux comprendre de nouvelles technologies en restant à jour et en allant toujours chercher de nouvelles connaissances',
      'J\'ai travaillé avec des outils comme JIRA pour afin de suivre des méthodologies Agile ou Kanban'
    ],
    education: [
      {
        title: 'Diplôme d\'études collégial en Informatique de gestion',
        location: 'CÉGEP de Drummondville',
        startDate: '2012',
        endDate: '2016'
      }
    ],
    realizations: [
      {
        name: 'Programmation de jeu vidéo',
        date: 'Depuis février 2022',
        description: 'Collaboration avec @Wamien pour la conception de son jeu vidéo, MegaRobots, et création d\'un jeu en une fin de semaine pour un Game Jam'
      },
      {
        name: 'Création de sites populaires pour les joueurs de Destiny 2',
        date: 'Depuis février 2015',
        description: 'Création d\'outils multiples, mais principalement de Time Wasted on Destiny, qui attire désormais plusieurs milliers de visiteurs par jour'
      },
    ],
    experience: [
      {
        title: 'Programmeur-Analyste',
        employer: 'TLM - Saguenay',
        startDate: '2021', // Octobre
        endDate: '2025', // Février
        achievements: [
          'Migration d\'applications monolithiques à des microservices distribués Cloud',
          'Optimisation d\'applications web, de services backend, et de requêtes SQL Server',
          'Analyse et suivi de projets en User Stories avec la méthodologie Agile',
          'Développement mobile multiplateforme avec Flutter, CarPlay et Android Auto',
          'Mise en place de déploiements automatisés sur Windows Server',
          'Déploiement d\'infrastructures AWS à-la-carte avec Terraform'
        ]
      },
      {
        title: 'Programmeur-Analyste',
        employer: 'Nmédia - Drummondville',
        startDate: '2016', // Mai
        endDate: '2021', // Octobre
        achievements: [
          'Analyse, organisation et suivi de projets en User Stories avec la méthodologie Agile',
          'Développement d\'applications web, d\'interfaces front-end et de services backend, avec tests automatisés',
          'Configuration d\'infrastructures Cloud et de déploiements automatisés',
          'Assistance de mes pairs avec des technologies émergentes et encore peu connues dans l\'organisation',
          'Prise en charge de projets "legacy" et apprentissage de langages moins modernes',
          'Recherche sur l\'innovation, exploration de nouvelles technologies et suivis avec mes pairs'
        ]
      }
    ],
    criteria: 'Ces diagrammes représentent mon appréciation de mes compétences dans mes domaines d\'expertise.'
  }
}

export const websites: Website[] = [
  {
    type: 'upcoming',
    name: 'CyberJay',
    icon: CyberJayIcon,
    startDate: 'Janvier 2025',
    endDate: '',
    techs: [techs.js, techs.twcss, techs.pcss, techs.docker],
    description: [
      'CyberJay est une startup de soutien informatique créée par un de mes amis. Bien que l\'entreprise ne soit pas encore lancée en affaires, j\'ai aidé à concevoir et à coder leur site Web pour débuter leur présence en ligne.',
      'Ce projet est en développement actif.'
    ]
  },
  {
    type: 'link',
    name: 'Carte de D&D',
    url: 'https://map.binar.ca',
    code: 'https://gitlab.com/binarmorker/tane-map',
    icon: DNDMapIcon,
    startDate: 'Décembre 2024',
    endDate: '',
    techs: [techs.js, techs.twcss, techs.pcss, techs.node, techs.exp, techs.docker],
    description: [
      'Ce site web se veut une sorte de navigation semblable à Google Maps, mais pour un monde de Donjons & Dragons que mes amis et moi entretenons pour nos parties. Il utilise un nuage de points placés manuellement, ainsi qu\'un simple serveur Express pour servir la liste de points et les liens entre eux.',
      'Ce projet est en développement actif.'
    ]
  },
  {
    type: 'archived',
    name: 'Application de saisie de données',
    icon: DataEntryIcon,
    startDate: 'Mai 2024',
    endDate: 'Mai 2024',
    techs: [techs.js, techs.twcss, techs.pcss, techs.elec],
    description: [
      'Un de mes amis voulait une application de bureau simple pour enregistrer les appels et enregistrer facilement les entrées dans un fichier CSV.',
      'Ce projet était un défi personnel puisqu\'ils l\'ont demandé avec 1 jour de retard.',
      'J\'ai livré et, avec des améliorations minimes, le lendemain, mon ami était satisfait.',
      'Ce projet est livré et définitif.'
    ]
  },
  {
    type: 'link',
    name: 'loot.report',
    url: 'https://loot.report',
    code: 'https://gitlab.com/wastedondestiny/loot.report',
    icon: LootReportIcon,
    startDate: 'Janvier 2024',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.docker],
    oldTechs: [techs.js],
    description: [
      'loot.report est un outil Web permettant de comparer l\'équipement entre les membres du clan dans le jeu vidéo Destiny 2 de Bungie.',
      'Le site Web est réalisé en Typescript, Vue et TailwindCSS, en utilisant Express et Puppeteer pour la génération statique pour le préchargement, et l\'API de Bungie pour récupérer des données en direct sur le jeu et les joueurs. Il utilise aussi Time Wasted sur le service backend de Destiny.',
      'Ce projet est en développement actif.'
    ]
  },
  {
    type: 'link',
    name: 'Répertoire Minecraft',
    url: 'https://minecraft.binar.ca/',
    icon: MinecraftRepoIcon,
    startDate: 'March 2024',
    endDate: '',
    techs: [techs.js, techs.java, techs.mysql, techs.docker, techs.linux],
    description: [
      'J\'ai commencé à héberger un serveur NAS au début de 2024, et c\'était l\'occasion pour moi de me lancer aussi dans l\'hébergement de jeux.',
      'J\'héberge présentement des serveurs de jeux pour des amis. Ça m\'apprend à allouer des ressources et à contrôler la demande sur du matériel réel.',
      'Je travaille toujours activement sur le serveur.'
    ]
  },
  {
    type: 'upcoming',
    name: 'Skillcraft Explorer',
    //url: 'https://skillcraft.binar.ca',
    code: 'https://gitlab.com/binarmorker/skillcraft',
    icon: SkillcraftExplorerIcon,
    startDate: 'Septembre 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.fb, techs.node, techs.exp, techs.pup],
    description: [
      'SkillCraft est un jeu de rôle sur table que mon ami @Utar94 a développé, et je voulais l\'aider, lui et nous, les joueurs, en créant une application Web pour gérer nos feuilles de personnage.',
      'Le site Web est réalisé avec Vue avec Typescript, Firebase, TailwindCSS et PostCSS.',
      'Il est présentement en développement occasionnel.'
    ]
  },
  {
    type: 'link',
    name: 'Light Companion',
    url: 'https://light.binar.ca',
    code: 'https://gitlab.com/binarmorker/light-companion',
    icon: LightCompanionIcon,
    startDate: 'Juin 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.fb, techs.node, techs.twcss, techs.pcss],
    oldTechs: [techs.js],
    description: [
      'Cet outil est un gestionnaire de feuilles de personnages et un planificateur de sessions pour Donjons et Destiny, une variante de la 5e édition de Dungeons & Dragons de Wizards of the Coast, le jeu de rôle sur table classique.',
      'L\'application Web est programmée en Typescript en utilisant Vue 3, Firebase, TailwindCSS et PostCSS.',
      'Il est présentement en développement occasionnel.'
    ]
  },
  {
    type: 'link',
    name: 'binar.ca - Site personnel',
    url: 'https://binar.ca',
    code: 'https://gitlab.com/binarmorker/portfolio',
    icon: BinarPortfolioIcon,
    startDate: 'Mai 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss],
    oldTechs: [techs.js, techs.bs],
    description: [
      'C\'est le site Web sur lequel vous vous trouvez actuellement. J\'ai fait ça pour présenter tous mes projets actuels et anciens.',
      'Il est programmé en Typescript en utilisant Vue 3, TailwindCSS et PostCSS.',
      'Il est présentement en développement occasionnel.'
    ]
  },
  {
    type: 'link',
    name: 'The Rogue Merchant',
    url: 'https://shop.binar.ca',
    code: 'https://gitlab.com/binarmorker/the-rogue-merchant',
    icon: TheRogueMerchantIcon,
    startDate: 'Février 2023',
    endDate: '',
    techs: [techs.js, techs.vue, techs.twcss, techs.pcss],
    oldTechs: [],
    description: [
      'Cette application d\'une seule page sert en fait des fichiers CSV et Markdown sous le capot. Codé en Vue 3 en Javascript et TailwindCSS, il sert de référence aux jeux Donjons et Dragons joués dans mes groupes.',
      'Les données sont compilées à partir de nombreuses années de documentation officielle et non officielle sur les coûts dans un monde médiéval fantastique.',
      'Ce projet est présentement en soutien à long terme.'
    ]
  },
  {
    type: 'link',
    name: 'crimson.report',
    url: 'https://crimson.report',
    code: 'https://gitlab.com/wastedondestiny/crimson.report',
    icon: CrimsonReportIcon,
    startDate: 'Février 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.docker],
    oldTechs: [techs.js],
    description: [
      'crimson.report est un outil Web permettant de vérifier la propriété des extensions, des saisons ou d\'autres droits qu\'un joueur pourrait avoir dans le jeu vidéo Destiny 2 de Bungie.',
      'Le site Web est réalisé en Typescript, Vue et TailwindCSS, en utilisant Express et Puppeteer pour la génération statique pour le préchargement, et l\'API de Bungie pour récupérer des données en direct sur le jeu et les joueurs. Il utilise aussi Time Wasted sur le service backend de Destiny.',
      'Ce projet est en développement actif.'
    ]
  },
  {
    type: 'download',
    name: 'Magic Mouth',
    url: 'https://npm.io/package/magicmouth',
    code: 'https://gitlab.com/binarmorker/magic-mouth',
    icon: MagicMouthIcon,
    startDate: 'Octobre 2022',
    endDate: '',
    techs: [techs.js, techs.djs, techs.node, techs.win, techs.linux],
    description: [
      'Cette application Javascript CLI est en fait un robot Discord qui achemine l\'audio de votre ordinateur vers votre canal vocal actuel. Invitez ce robot et voyez à quel point le son est clair!',
      'À l\'origine, ce robot avait pour but de diffuser de la musique et des effets sonores libres de droits pour les jeux en ligne Donjons & Dragons, c\'est pourquoi il partage son nom avec un sort magique.',
      'Ce projet est présentement en soutien à long terme.'
    ]
  },
  {
    type: 'link',
    name: 'Kenney Jam 2022',
    url: 'https://binarmorker.itch.io/mom-wants-me-to-take-care-of-her-plants-and-i-dont-know-what-to-do',
    icon: KenneyJamIcon,
    startDate: 'Juillet 2022',
    endDate: 'Juillet 2022',
    techs: [techs.unity, techs.dotnet],
    description: [
      'Avec l\'aide de @Wamien, j\'ai créé un jeu vidéo en une seule fin de semaine et soumis la demande à temps pour le Kenney Jam 2022.',
      'Le thème était la croissance, alors on a réfléchi au concept de faire pousser des plantes dans un jardin, dans un laps de temps limité, et d\'amasser de l\'argent pour acheter plus de graines.',
      'Ce projet est terminé et téléchargeable sur itch.io.'
    ]
  },
  {
    type: 'link',
    name: 'Destiny Season Predictions',
    url: 'https://predict.wastedondestiny.com',
    code: 'https://gitlab.com/wastedondestiny/prediction-engine-s19',
    icon: DestinySeasonPredictionsIcon,
    startDate: 'Mai 2022',
    endDate: 'Décembre 2022',
    techs: [techs.js, techs.vue, techs.exp, techs.pup],
    oldTechs: [],
    description: [
      'Le contenu saisonnier de Destiny 2 n\'est pas souvent annoncé à l\'avance, c\'est pourquoi j\'ai créé ce site Web, fortement inspiré par le travail d\'un autre de mes amis : @FreddyAlabastro. Il a fait le design, créé les images et supervisé le travail pendant que j\'intégrais le tout dans un petit jeu de devinettes en ligne.',
      'Cette application très simple d\'une seule page utilise Javascript et Vue, avec génération statique avec Express et Puppeteer. L\'enregistrement de votre prédiction se fait à l\'aide d\'un point de terminaison sur le service backend Time Wasted on Destiny.',
      'Ce projet est présentement retiré.'
    ]
  },
  {
    type: 'archived',
    name: 'Exalted Manager',
    //url: 'https://exalted.binar.ca',
    //code: 'https://gitlab.com/binarmorker/exaltedmanager',
    icon: ExaltedManagerIcon,
    startDate: 'Septembre 2021',
    endDate: 'Juin 2022',
    techs: [techs.js, techs.vue, techs.vtfy, techs.fb, techs.nuxt, techs.exp, techs.node, techs.sass, techs.linux],
    description: [
      'Exalted 3rd Edition est un autre jeu de rôle sur table dans lequel je me suis plongé et que j\'ai trouvé suffisamment déroutant pour vouloir un moyen simple de suivre les progrès et les choix.',
      'Ce site Web était ma tentative d\'utiliser Nuxt, associé à Vuetify, Express et Sass.',
      'Il est présentement retiré.'
    ]
  },
  {
    type: 'download',
    name: 'API MegaRobots',
    url: 'https://wamien.itch.io/mega-robot',
    //code: 'https://gitlab.com/binarmorker/megarobots-api',
    icon: MegaRobotsAPIIcon,
    startDate: 'Juin 2021',
    endDate: '',
    techs: [techs.ts, techs.fb, techs.exp, techs.node, techs.dotnet, techs.unity, techs.docker],
    description: [
      'MegaRobots est un jeu créé par mon bon ami @Wamien. Son projet avait besoin d\'un système de sauvegarde back-end et d\'une base de données centralisée. En utilisant Typescript, Express et Firebase, j\'ai créé une API simple pour faciliter son accès à la programmation backend.',
      'J\'ai cédé la propriété à @Wamien pour ce projet.'
    ]
  },
  {
    type: 'archived',
    name: 'Gallerie Web 2',
    //url: 'https://gallery2.binar.ca',
    //code: 'https://gitlab.com/binarmorker/genshin-hentai',
    icon: WebGallery2Icon,
    startDate: 'Décembre 2020',
    endDate: 'Novembre 2023',
    techs: [techs.php, techs.js, techs.vue, techs.sqlite, techs.sass, techs.linux],
    oldTechs: [],
    description: [
      'Cette galerie Web était censée être un site Web très simple de présentation et de filtrage de portfolios, mais elle n\'a jamais vu le jour. J\'ai écrit ceci en Javascript en utilisant Vue et Sass, mais j\'ai vite réalisé que j\'avais besoin d\'un backend et j\'ai opté pour PHP et SQLite.',
      'Ce projet est présentement retiré.'
    ]
  },
  {
    type: 'link',
    name: 'Oubliette',
    url: 'https://app.oubliette.xyz',
    code: 'https://gitlab.com/oubliette/app',
    icon: OublietteXYZIcon,
    startDate: 'Mars 2020',
    endDate: 'Juin 2022',
    techs: [techs.js, techs.vue, techs.elec, techs.docker, techs.linux],
    oldTechs: [techs.node, techs.dotnet, techs.win],
    description: [
      'Oubliette est une application de gestion de personnages pour Donjons et Dragons, réalisée en Javascript et Vue. Le site Web permet de sauvegarder, de collaborer et même de jouer avec des feuilles de personnages créées par les joueurs. J\'ai dû coordonner avec mon ami @Utar94 (qui a créé le backend en utilisant PostgreSQL et .NET) et créer une application frontend pour refléter sa structure et les modifications de ses données.',
      'Le site Web était également disponible localement, pour être complètement hors ligne avec l\'aide d\'Electron et d\'une version plus petite du backend, mais cela n\'a été que partiellement réalisé avant l\'arrêt du projet. Cependant, j\'ai quand même programmé toute la couche Electron.',
      'Ce projet est présentement retiré.'
    ]
  },
  {
    type: 'link',
    name: 'Lancez Initiative',
    url: 'https://roll.binar.ca',
    code: 'https://gitlab.com/binarmorker/initiative',
    icon: RollForInitiativeIcon,
    startDate: 'Mars 2019',
    endDate: '',
    techs: [techs.js, techs.vue, techs.vtfy, techs.docker],
    description: [
      'Une application d\'une seule page utilisant la puissance du stockage local du navigateur pour partager l\'état entre deux fenêtres ouvertes. Lancez Initiative est un petit projet réalisé avec Javascript et Vue, utilisant Vuetify pour son style.',
      'Le but de l\'application est de faciliter le suivi des initiatives dans n\'importe quel jeu de rôle sur table, tout en permettant la gestion du statut des créatures et des joueurs, des PV et du groupe.',
      'Ce projet est présentement en soutien à long terme.'
    ]
  },
  {
    type: 'archived',
    name: 'Zelda Gear',
    //url: 'https://zelda.binar.ca',
    //code: 'https://gitlab.com/binarmorker/ZeldaGear',
    icon: ZeldaGearIcon,
    startDate: 'Janvier 2018',
    endDate: 'Mars 2018',
    techs: [techs.js, techs.ko, techs.bs],
    description: [
      'Un outil simple pour vérifier de quel équipement vous disposez et de quels matériaux vous avez besoin pour les améliorer dans le jeu vidéo de Nintendo The Legend of Zelda: Breath of the Wild.',
      'Il a été écrit en Javascript en utilisant Knockout.js pour la réactivité et Bootstrap pour le style.',
      'Le projet a été abandonné avant d\'être complètement achevé.'
    ]
  },
  {
    type: 'link',
    name: 'Time Wasted on Destiny',
    url: 'https://wastedondestiny.com',
    code: 'https://gitlab.com/wastedondestiny/new-website',
    icon: WastedOnDestinyIcon,
    startDate: 'Septembre 2017',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.node, techs.mysql, techs.docker],
    oldTechs: [techs.php, techs.sqlite, techs.cf, techs.dotnet, techs.js, techs.bs, techs.linux],
    description: [
      'Time Wasted on Destiny est une page Web utilisée pour consulter le temps joué sur les personnages existants et supprimés sur Destiny et Destiny 2. L\'outil détaille également d\'autres réalisations et propose un classement.',
      'Le site Web est réalisé en Typescript, Vue et TailwindCSS, en utilisant Express et Puppeteer pour la génération statique pour le préchargement, et l\'API de Bungie pour récupérer des données en direct sur le jeu et les joueurs. Le backend est maintenant un Worker node.js écrit en Typescript qui utilise une base de données MariaDB pour stocker les données du classement.',
      'Ce projet a connu de nombreuses itérations au fil des ans, étant initialement réalisé avec Bootstrap et PHP. Il a subi plusieurs refactorisations et réécritures avant que je me décide finalement pour cette version.',
      'Il est toujours en développement actif.'
    ]
  },
  {
    type: 'link',
    name: 'Sayonara Memories',
    url: 'https://sayonara.binar.ca',
    code: 'https://gitlab.com/binarmorker/SayonaraMemories',
    icon: SayonaraMemoriesIcon,
    startDate: 'Mai 2017',
    endDate: 'Janvier 2020',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.ko, techs.less],
    oldTechs: [],
    description: [
      'Ce forum avait pour but de remplacer les anciens PhpBB pour les jeux de rôle. Il a été écrit en PHP et MySQL côté backend, et en Javascript, Knockout.js, Bootstrap et Less côté frontend.',
      'Le projet a été un bon exercice de gestion de produit, ainsi que de coordination du temps et de l\'équipe. Il est présentement retiré.'
    ]
  },
  {
    type: 'archived',
    name: 'Feuille de Temps Entreprise',
    //url: '',
    //code: 'https://bitbucket.org/Youmy/indbags_time-tracker',
    icon: ETimesheetIcon,
    startDate: 'Juin 2016',
    endDate: 'Janvier 2017',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.ko, techs.win],
    description: [
      'Ce projet a été réalisé pour un client qui avait besoin d\'un moyen simple pour que ses employés enregistrent leur temps et que les gestionnaires puissent suivre les entrées et les sorties.',
      'Ils étaient satisfaits du logiciel qu\'ils avaient reçu et l\'ont même intégré par la suite à leur logiciel de comptabilité.',
      'Ce projet est maintenant retiré.'
    ]
  },
  {
    type: 'archived',
    name: 'Gallerie Web 1',
    //url: 'https://gallery.binar.ca',
    //code: 'https://bitbucket.org/binarmorker/o-bar-18',
    icon: WebGallery1Icon,
    startDate: 'Juin 2016',
    endDate: 'Janvier 2020',
    techs: [techs.php, techs.js, techs.fb, techs.mysql, techs.ko, techs.less, techs.linux],
    description: [
      'Cette galerie Web était censée offrir une expérience complète de contenu généré par les utilisateurs et s\'adressait à une communauté d\'environ 50 personnes. J\'ai écrit le projet en Javascript en utilisant Knockout.js et Less, et implémenté le backend avec PHP et MySQL, tout en utilisant Firebase pour les notifications.',
      'Ce projet est maintenant retiré.'
    ]
  },
  {
    type: 'download',
    name: 'TinyTemplate',
    url: 'https://packagist.org/packages/binarmorker/tinytemplate',
    code: 'https://gitlab.com/binarmorker/TinyTemplate',
    icon: TinyTemplateIcon,
    startDate: 'Avril 2016',
    endDate: 'Janvier 2017',
    techs: [techs.php],
    description: [
      'Ce paquet PHP est un système de modèles simple utilisant des accolades dans HTML pour générer du contenu de manière dynamique.',
      'Le projet est abandonné, mais toujours disponible en téléchargement.'
    ]
  },
  {
    type: 'download',
    name: 'APIne Framework',
    url: 'https://packagist.org/packages/apinephp/legacy-framework',
    code: 'https://gitlab.com/apinephp/legacy-framework',
    icon: APIneIcon,
    startDate: 'Février 2015',
    endDate: 'Décembre 2018',
    techs: [techs.php],
    description: [
      'Un cadre ambitieux répondant aux besoins de deux développeurs qui voulaient créer des API et des sites Web à part entière avec du routage, des modules et un modèle MVC. APIne a été utilisé sur la plupart de mes projets jusqu\'en 2018, date à laquelle j\'ai commencé à me spécialiser en Javascript. Bien que la majeure partie du cadre ait été réalisée par mon ami @Youmy001, j\'ai apporté mes propres améliorations et fonctionnalités.',
      'Le projet est maintenant abandonné et même si une nouvelle version était en cours de développement par @Youmy001 depuis un certain temps, elle n\'est plus maintenue.'
    ]
  },
  {
    type: 'link',
    name: 'Small Name',
    url: 'https://smallname.binar.ca',
    code: 'https://gitlab.com/binarmorker/Small-Name',
    icon: SmallNameIcon,
    startDate: 'Février 2016',
    endDate: 'Janvier 2020',
    techs: [techs.php, techs.mysql, techs.bs],
    description: [
      'Mon avis sur un réducteur d\'URL, à l\'époque où il était utile d\'en avoir un à portée de la main. Le projet utilise un algorithme pseudo-aléatoire pour générer des hachages basés sur des nombres séquentiels. Le site Web est construit en utilisant PHP, MySQL et Bootstrap.',
      'Il est présentement retiré.'
    ]
  },
  {
    type: 'link',
    name: 'Dinklebot',
    url: 'https://dinklebot.binar.ca',
    code: 'https://gitlab.com/binarmorker/Dinklebot',
    icon: DinklebotNetIcon,
    startDate: 'Février 2015',
    endDate: 'Septembre 2017',
    techs: [techs.php, techs.mysql, techs.bs, techs.linux],
    description: [
      'Destiny a toujours été un jeu de frime, et ce site Web a été fait pour cela. Il remplit également le rôle d\'être un bon compagnon de jeu pour les finalistes et les broyeurs hebdomadaires.',
      'Dinklebot est créé avec PHP, MySQL et Bootstrap.',
      'Il est présentement retiré.'
    ]
  },
  {
    type: 'archived',
    name: 'CRM Entreprise',
    //url: '',
    //code: 'https://bitbucket.org/Youmy/indbags_forum',
    icon: EForumIcon,
    startDate: 'Février 2015',
    endDate: 'Janvier 2017',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.win],
    description: [
      'Développé pour un client qui avait besoin de synchroniser les demandes de produits des employés entre les pays.',
      'Au départ, en utilisant Drupal et d\'autres logiciels de forum, nous avons réussi à créer ce qui nous semblait familier, tout en étant adapté à leurs besoins.',
      'Il est présentement à la retraite.'
    ]
  },
  {
    type: 'download',
    name: 'War of Raekidion',
    url: '/download/raekidion_0.7.5_windows.exe',
    code: 'https://bitbucket.org/Learz/raekidion',
    icon: WarOfRaekidionIcon,
    startDate: 'Janvier 2014',
    endDate: 'Août 2014',
    techs: [techs.eiffel, techs.sdl2],
    description: [
      'J\'ai créé ce jeu avec mon ami @Learz quand j\'étais à l\'université, et on a beaucoup appris sur la création de jeux et la programmation basée sur des contrats pendant cette période.',
      'Il est programmé en Eiffel, un langage de programmation unique qui nous a permis d\'encapsuler SDL 2 et de l\'utiliser pour afficher des sprites.',
      'Le jeu est toujours téléchargeable, mais le projet est abandonné.'
    ]
  },
  {
    type: 'download',
    name: 'NES Blob',
    url: '/download/Unnamed_V0.6.0.nes',
    code: 'https://gitlab.com/binarmorker/NES_Blob',
    icon: NESBlobIcon,
    startDate: 'Janvier 2014',
    endDate: 'Août 2014',
    techs: [techs.asm],
    description: [
      'J\'ai créé ce jeu en 6502 Assembly pour la NES quand j\'étais à l\'université. En faisant cela, j\'ai acquis beaucoup de connaissances sur le processus de compilation, la décompilation, l\'utilisation des registres et des interruptions, et le calcul en binaire et hexadécimal.',
      'Le jeu devrait fonctionner sur une console NES ou Famicom, à condition que vous ayez une cartouche programmable. Cependant, il est plus simple de jouer sur un émulateur.',
      'Le jeu est toujours téléchargeable, mais le projet est abandonné.'
    ]
  },
]
