import { type Resume, type Website, resumePartial, techs } from './index'

import DNDMapIcon from '../../components/icons/DNDMap.vue'
import KenneyJamIcon from '../../components/icons/KenneyJam.vue'
import SkillcraftExplorerIcon from '../../components/icons/SkillcraftExplorer.vue'
import DataEntryIcon from '@/components/icons/DataEntry.vue'
import EForumIcon from '../../components/icons/EForum.vue'
import ETimesheetIcon from '../../components/icons/ETimesheet.vue'
import CyberJayIcon from '../../components/icons/CyberJay.vue'
import BinarPortfolioIcon from '../../components/icons/BinarPortfolio.vue'
import MinecraftRepoIcon from '../../components/icons/MinecraftRepo.vue'
import LootReportIcon from '../../components/icons/LootReport.vue'
import LightCompanionIcon from '../../components/icons/LightCompanion.vue'
import TheRogueMerchantIcon from '../../components/icons/TheRogueMerchant.vue'
import MagicMouthIcon from '../../components/icons/MagicMouth.vue'
import CrimsonReportIcon from '../../components/icons/CrimsonReport.vue'
import MegaRobotsAPIIcon from '../../components/icons/MegaRobotsAPI.vue'
import DestinySeasonPredictionsIcon from '../../components/icons/DestinySeasonPredictions.vue'
import WastedOnDestinyIcon from '../../components/icons/WastedOnDestiny.vue'
import OublietteXYZIcon from '../../components/icons/OublietteXYZ.vue'
import WebGallery2Icon from '../../components/icons/WebGallery2.vue'
import WebGallery1Icon from '../../components/icons/WebGallery1.vue'
import ExaltedManagerIcon from '../../components/icons/ExaltedManager.vue'
import RollForInitiativeIcon from '../../components/icons/RollForInitiative.vue'
import ZeldaGearIcon from '../../components/icons/ZeldaGear.vue'
import SayonaraMemoriesIcon from '../../components/icons/SayonaraMemories.vue'
import TinyTemplateIcon from '../../components/icons/TinyTemplate.vue'
import APIneIcon from '../../components/icons/APIne.vue'
import SmallNameIcon from '../../components/icons/SmallName.vue'
import DinklebotNetIcon from '../../components/icons/DinklebotNet.vue'
import WarOfRaekidionIcon from '../../components/icons/WarOfRaekidion.vue'
import NESBlobIcon from '../../components/icons/NESBlob.vue'

export const resume: Resume = {
  ...resumePartial,
  ...{
    name: 'François Allard',
    summary: 'I am a web developer passionate about innovation and new technologies. I don\'t hesitate to point out potential obstacles and look for solutions before they become problems. I believe that learning and sharing our knowledge is what helps us grow the most. I am a self-critical, self-taught, and persevering person.',
    achievements: [
      'I have transformed hard-to-maintain applications into smaller, more manageable services',
      'I have helped my peers better understand new technologies by staying up-to-date and always seeking new knowledge',
      'I have worked with tools like JIRA to follow Agile or Kanban methodologies'
    ],
    education: [
      {
        title: 'Diploma of College Studies in Business Computing',
        location: 'CÉGEP de Drummondville',
        startDate: '2012',
        endDate: '2016'
      }
    ],
    realizations: [
      {
        name: 'Programmation de jeu vidéo',
        date: 'Since February 2022',
        description: 'Collaboration with @Wamien on the design of his video game, MegaRobots, and creation of a game in one weekend for a Game Jam'
      },
      {
        name: 'Creation of popular websites for Destiny 2 players',
        date: 'Since February 2015',
        description: 'Creation of multiple tools, but primarily Time Wasted on Destiny, which now attracts several thousand visitors per day'
      },
    ],
    experience: [
      {
        title: 'Programmer-Analyst',
        employer: 'TLM - Saguenay',
        startDate: '2021', // Octobre
        endDate: '2025', // Février
        achievements: [
          'Migration of monolithic applications to distributed Cloud microservices',
          'Optimization of web applications, backend services, and SQL Server queries',
          'Analysis and tracking of projects in User Stories using Agile methodology',
          'Cross-platform mobile development with Flutter, CarPlay, and Android Auto',
          'Implementation of automated deployments on Windows Server',
          'Deployment of on-demand AWS infrastructures using Terraform'
        ]
      },
      {
        title: 'Programmer-Analyst',
        employer: 'Nmédia - Drummondville',
        startDate: '2016', // Mai
        endDate: '2021', // Octobre
        achievements: [
          'Analysis, organization, and tracking of projects in User Stories using Agile methodology',
          'Development of web applications, front-end interfaces, and backend services, with automated testing',
          'Configuration of Cloud infrastructures and automated deployments',
          'Assisting peers with emerging technologies still unfamiliar within the organization',
          'Taking charge of "legacy" projects and learning older languages',
          'Research on innovation, exploration of new technologies, and follow-ups with peers'
        ]
      }
    ],
    criteria: 'These diagrams represent my assessment of my skills in my areas of expertise.'
  }
}

export const websites: Website[] = [
  {
    type: 'upcoming',
    name: 'CyberJay',
    icon: CyberJayIcon,
    startDate: 'January 2025',
    endDate: '',
    techs: [techs.js, techs.twcss, techs.pcss, techs.docker],
    description: [
      'CyberJay is a startup IT support company started by a friend of mine. Even though the business is not yet started, I helped design and code their website to kickstart their online presence.',
      'This project is in active development.'
    ]
  },
  {
    type: 'link',
    name: 'D&D Map',
    url: 'https://map.binar.ca',
    code: 'https://gitlab.com/binarmorker/tane-map',
    icon: DNDMapIcon,
    startDate: 'December 2024',
    endDate: '',
    techs: [techs.js, techs.twcss, techs.pcss, techs.node, techs.exp, techs.docker],
    description: [
      'This website serves as a sort of nagivation system like Google Maps, but for a Dungeons & Dragons world that my friends and I use for our games. It uses a manually attributed point cloud and a simple Express server to serve the points and links between them.',
      'This project is in active development.'
    ]
  },
  {
    type: 'archived',
    name: 'Data Entry App',
    icon: DataEntryIcon,
    startDate: 'May 2024',
    endDate: 'May 2024',
    techs: [techs.js, techs.twcss, techs.pcss, techs.elec],
    description: [
      'A friend of mine wanted a simple desktop application to register calls and easily log entries to a CSV file.',
      'This project was a personal challenge since they asked for this on a 1 day delay.',
      'I delivered, and with minimal improvements the following day, my friend was satisfied.',
      'This project is delivered and final.'
    ]
  },
  {
    type: 'link',
    name: 'loot.report',
    url: 'https://loot.report',
    code: 'https://gitlab.com/wastedondestiny/loot.report',
    icon: LootReportIcon,
    startDate: 'January 2024',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.docker],
    oldTechs: [techs.js],
    description: [
      'loot.report is a web tool to compare equipment between clan members in Bungie\'s video game Destiny 2. Viewing this data allows for experienced players to plan grouped activities together with clanmates who lack certain items, so they can have a chance to get them.',
      'The website is made in Typescript, Vue and TailwindCSS, using Express and Puppeteer for static generation for preloading, and Bungie\'s API to fetch live data on the game and players. It also uses Time Wasted on Destiny\'s backend service.',
      'This project is in active development.'
    ]
  },
  {
    type: 'link',
    name: 'Minecraft Repository',
    url: 'https://minecraft.binar.ca/',
    icon: MinecraftRepoIcon,
    startDate: 'March 2024',
    endDate: '',
    techs: [techs.js, techs.java, techs.mysql, techs.docker, techs.linux],
    description: [
      'I started hosting a NAS server at the beginning of 2024, and it was the occasion for me to start hosting games too.',
      'I\'m currently hosting game servers for some friends. It teaches me how to allocate resource and control demand on real hardware.',
      'I am still actively working on the server.'
    ]
  },
  {
    type: 'upcoming',
    name: 'Skillcraft Explorer',
    //url: 'https://skillcraft.binar.ca',
    code: 'https://gitlab.com/binarmorker/skillcraft',
    icon: SkillcraftExplorerIcon,
    startDate: 'September 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.fb, techs.node, techs.exp, techs.pup],
    description: [
      'SkillCraft is a tabletop role-playing game that my friend @Utar94 has developed, and I wanted to help him and us players by making a web app to manage our character sheets.',
      'The website is made using Vue with Typescript, Firebase, TailwindCSS and PostCSS.',
      'It is currently in occasional development.'
    ]
  },
  {
    type: 'link',
    name: 'Light Companion',
    url: 'https://light.binar.ca',
    code: 'https://gitlab.com/binarmorker/light-companion',
    icon: LightCompanionIcon,
    startDate: 'June 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.fb, techs.node, techs.twcss, techs.pcss],
    oldTechs: [techs.js],
    description: [
      'This tool is a character sheet manager and session planner for Dungeons & Destiny, a variant of Wizards of the Coast\'s Dungeons & Dragons 5th edition, the classic tabletop role-playing game.',
      'The web application is programmed in Typescript using Vue 3, Firebase, TailwindCSS and PostCSS.',
      'It is currently in occasional development.'
    ]
  },
  {
    type: 'link',
    name: 'binar.ca - Personal site',
    url: 'https://binar.ca',
    code: 'https://gitlab.com/binarmorker/portfolio',
    icon: BinarPortfolioIcon,
    startDate: 'May 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss],
    oldTechs: [techs.js, techs.bs],
    description: [
      'This is the website you are on right now. I made it to showcase all my current and old projects.',
      'It is programmed in Typescript using Vue 3, TailwindCSS and PostCSS.',
      'It is currently in occasional development.'
    ]
  },
  {
    type: 'link',
    name: 'The Rogue Merchant',
    url: 'https://shop.binar.ca',
    code: 'https://gitlab.com/binarmorker/the-rogue-merchant',
    icon: TheRogueMerchantIcon,
    startDate: 'February 2023',
    endDate: '',
    techs: [techs.js, techs.vue, techs.twcss, techs.pcss],
    oldTechs: [],
    description: [
      'This single-page application is actually serving CSV and Markdown files under the hood. Coded in Vue 3 using Javascript and TailwindCSS, it serves as a reference for Dungeons & Dragons games played in my groups.',
      'The data is compiled from many years of official and unofficial documentation of cost in a medieval fantasy world.',
      'This project is currently in long-term support.'
    ]
  },
  {
    type: 'link',
    name: 'crimson.report',
    url: 'https://crimson.report',
    code: 'https://gitlab.com/wastedondestiny/crimson.report',
    icon: CrimsonReportIcon,
    startDate: 'February 2023',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.docker],
    oldTechs: [techs.js],
    description: [
      'crimson.report is a web tool to verify ownership of expansions, seasons or other entitlements a player might have in Bungie\'s video game Destiny 2. Viewing this data allows players to compare their list of available activities to play together.',
      'The website is made in Typescript, Vue and TailwindCSS, using Express and Puppeteer for static generation for preloading, and Bungie\'s API to fetch live data on the game and players. It also uses Time Wasted on Destiny\'s backend service.',
      'This project is in active development.'
    ]
  },
  {
    type: 'download',
    name: 'Magic Mouth',
    url: 'https://npm.io/package/magicmouth',
    code: 'https://gitlab.com/binarmorker/magic-mouth',
    icon: MagicMouthIcon,
    startDate: 'October 2022',
    endDate: '',
    techs: [techs.js, techs.djs, techs.node, techs.win, techs.linux],
    description: [
      'This Javascript CLI application is actually a Discord bot that routes audio from your computer to your current voice channel. Invite this bot and see how crisp the sound is!',
      'Originally, this bot served the purpose of playing copyright-free music and sound effects for online Dungeons & Dragons games, which is why it shares its name with a magic spell.',
      'This project is in long-term support.'
    ]
  },
  {
    type: 'link',
    name: 'Kenney Jam 2022',
    url: 'https://binarmorker.itch.io/mom-wants-me-to-take-care-of-her-plants-and-i-dont-know-what-to-do',
    icon: KenneyJamIcon,
    startDate: 'July 2022',
    endDate: 'July 2022',
    techs: [techs.unity, techs.dotnet],
    description: [
      'With the help of @Wamien, I created a videogame in a single weekend, and submitted the entry in time for the Kenney Jam 2022.',
      'The theme was Growth, so we thinkered around with the concept of both growing plants in a garden, in a limited amount of time, and growing money to buy more seeds.',
      'This project is finished and downloadable on itch.io.'
    ]
  },
  {
    type: 'link',
    name: 'Destiny Season Predictions',
    url: 'https://predict.wastedondestiny.com',
    code: 'https://gitlab.com/wastedondestiny/prediction-engine-s19',
    icon: DestinySeasonPredictionsIcon,
    startDate: 'May 2022',
    endDate: 'December 2022',
    techs: [techs.js, techs.vue, techs.exp, techs.pup],
    oldTechs: [],
    description: [
      'Destiny 2 seasonal content is not often announced in advance, which is why I made this website, heavily inspired by the work of another friend of mine: @FreddyAlabastro. He made the design, created images and supervised the work while I integrated it all into a little online guessing game.',
      'This very simple single-page app uses Javascript and Vue, with static generation with Express and Puppeteer. Saving your prediction is done using an endpoint on Time Wasted on Destiny\'s backend service.',
      'This project is retired.'
    ]
  },
  {
    type: 'archived',
    name: 'Exalted Manager',
    //url: 'https://exalted.binar.ca',
    //code: 'https://gitlab.com/binarmorker/exaltedmanager',
    icon: ExaltedManagerIcon,
    startDate: 'September 2021',
    endDate: 'June 2022',
    techs: [techs.js, techs.vue, techs.vtfy, techs.fb, techs.nuxt, techs.exp, techs.node, techs.sass, techs.linux],
    description: [
      'Exalted 3rd Edition is another tabletop role-playing game that I delved into and found confusing enough that I wanted an easy way to track progress and choices.',
      'This website was my attempt at using Nuxt, paired with Vuetify, Express and Sass.',
      'It is currently retired.'
    ]
  },
  {
    type: 'download',
    name: 'MegaRobots API',
    url: 'https://wamien.itch.io/mega-robot',
    //code: 'https://gitlab.com/binarmorker/megarobots-api',
    icon: MegaRobotsAPIIcon,
    startDate: 'June 2021',
    endDate: '',
    techs: [techs.ts, techs.fb, techs.exp, techs.node, techs.dotnet, techs.unity, techs.docker],
    description: [
      'MegaRobots is a game made by my good friend @Wamien. His project needed a backend saving system and a centralized database. Using Typescript, Express and Firebase, I built a simple API to ease his way into backend programming.',
      'I have given ownership to @Wamien for this project.'
    ]
  },
  {
    type: 'archived',
    name: 'Web Gallery 2',
    //url: 'https://gallery2.binar.ca',
    //code: 'https://gitlab.com/binarmorker/genshin-hentai',
    icon: WebGallery2Icon,
    startDate: 'December 2020',
    endDate: 'November 2023',
    techs: [techs.php, techs.js, techs.vue, techs.sqlite, techs.sass, techs.linux],
    oldTechs: [],
    description: [
      'This web gallery was supposed to be a very simple showcase and filtering website for portfolios, but it never came to be. I wrote this in Javascript using Vue and Sass, but quickly realized I needed a backend and opted for PHP and SQLite.',
      'This project is now retired.'
    ]
  },
  {
    type: 'link',
    name: 'Oubliette',
    url: 'https://app.oubliette.xyz',
    code: 'https://gitlab.com/oubliette/app',
    icon: OublietteXYZIcon,
    startDate: 'March 2020',
    endDate: 'June 2022',
    techs: [techs.js, techs.vue, techs.elec, techs.docker, techs.linux],
    oldTechs: [techs.node, techs.dotnet, techs.win],
    description: [
      'Oubliette is a character management app for Dungeons & Dragons, made using Javascript and Vue. The website allows saving, collaborating, and even playing with character sheets created by players. I had to coordinate with my friend @Utar94 (who made the backend using PostgreSQL and .NET) and create a frontend application to reflect his structure and data changes.',
      'The website was also available locally, to be made fully offline with the help of Electron and a smaller version of the backend, but that was only partially done before the project was discontinued. I still programmed all the Electron layer, though.',
      'This project is currently retired.'
    ]
  },
  {
    type: 'link',
    name: 'Roll Initiative',
    url: 'https://roll.binar.ca',
    code: 'https://gitlab.com/binarmorker/initiative',
    icon: RollForInitiativeIcon,
    startDate: 'March 2019',
    endDate: '',
    techs: [techs.js, techs.vue, techs.vtfy, techs.docker],
    description: [
      'A single-page application using the power of the browser\'s local storage to share state between two open windows. Roll Initiative is a small project made with Javascript and Vue, using Vuetify for it\'s styling.',
      'The app\'s goal is to make initiative tracking easy in any tabletop role-playing game, while allowing creature and player status, HP and party management.',
      'It is currently in long-term support.'
    ]
  },
  {
    type: 'archived',
    name: 'Zelda Gear',
    //url: 'https://zelda.binar.ca',
    //code: 'https://gitlab.com/binarmorker/ZeldaGear',
    icon: ZeldaGearIcon,
    startDate: 'January 2018',
    endDate: 'March 2018',
    techs: [techs.js, techs.ko, techs.bs],
    description: [
      'A simple tool to check what equipment you have and what materials you need to upgrade them in Nintendo\'s videogame The Legend of Zelda: Breath of the Wild.',
      'It was written in Javascript using Knockout.js for reactivity and Bootstrap for styling.',
      'The project has been retired before being fully completed.'
    ]
  },
  {
    type: 'link',
    name: 'Time Wasted on Destiny',
    url: 'https://wastedondestiny.com',
    code: 'https://gitlab.com/wastedondestiny/new-website',
    icon: WastedOnDestinyIcon,
    startDate: 'September 2017',
    endDate: '',
    techs: [techs.ts, techs.vue, techs.twcss, techs.pcss, techs.exp, techs.pup, techs.node, techs.mysql, techs.docker],
    oldTechs: [techs.php, techs.sqlite, techs.cf, techs.dotnet, techs.js, techs.bs, techs.linux],
    description: [
      'Time Wasted on Destiny is a web page used to look at time played on existing and deleted characters on Destiny and Destiny 2. The tool also details other achievements and features a leaderboard.',
      'The website is made in Typescript, Vue and TailwindCSS, using Express and Puppeteer for static generation for preloading, and Bungie\'s API to fetch live data on the game and players. The backend is now a node.js Worker written in Typescript which uses a MariaDB database to store leaderboard data.',
      'This project has seen numerous iterations through the years, being initially made with Bootstrap and PHP. It underwent several refactors and rewrites before I finally settled on this version.',
      'It is still in active development.'
    ]
  },
  {
    type: 'link',
    name: 'Sayonara Memories',
    url: 'https://sayonara.binar.ca',
    code: 'https://gitlab.com/binarmorker/SayonaraMemories',
    icon: SayonaraMemoriesIcon,
    startDate: 'May 2017',
    endDate: 'January 2020',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.ko, techs.less],
    oldTechs: [],
    description: [
      'This forum was aimed to be a replacement to old PhpBBs for role-playing games. It was written in PHP and MySQL on the backend side, and Javascript, Knockout.js, Bootstrap and Less on the frontend side.',
      'The project was a good exercise in product management, as well as time and team coordination. It is currently retired.'
    ]
  },
  {
    type: 'archived',
    name: 'Enterprise Timesheet',
    //url: '',
    //code: 'https://bitbucket.org/Youmy/indbags_time-tracker',
    icon: ETimesheetIcon,
    startDate: 'June 2016',
    endDate: 'January 2017',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.ko, techs.win],
    description: [
      'This project was made for a client who needed an easy way for his employees to log their time, and for the managers to track ins and outs.',
      'They were happy with the software they got and even got it integrated to their accounting software afterwards.',
      'This project is now retired.'
    ]
  },
  {
    type: 'archived',
    name: 'Web Gallery 1',
    //url: 'https://gallery.binar.ca',
    //code: 'https://bitbucket.org/binarmorker/o-bar-18',
    icon: WebGallery1Icon,
    startDate: 'June 2016',
    endDate: 'January 2020',
    techs: [techs.php, techs.js, techs.fb, techs.mysql, techs.ko, techs.less, techs.linux],
    description: [
      'This web gallery was supposed to be a complete user-generated content experience, and catered to a community of about 50 people. I wrote the project in Javascript using Knockout.js and Less, and implemented the backend with PHP and MySQL, while using Firebase for notifications.',
      'This project is now retired.'
    ]
  },
  {
    type: 'download',
    name: 'TinyTemplate',
    url: 'https://packagist.org/packages/binarmorker/tinytemplate',
    code: 'https://gitlab.com/binarmorker/TinyTemplate',
    icon: TinyTemplateIcon,
    startDate: 'April 2016',
    endDate: 'January 2017',
    techs: [techs.php],
    description: [
      'This PHP package is a simple templating system using curly brackets inside HTML to generate content dynamically.',
      'The project is abandoned, but still available to download.'
    ]
  },
  {
    type: 'download',
    name: 'APIne Framework',
    url: 'https://packagist.org/packages/apinephp/legacy-framework',
    code: 'https://gitlab.com/apinephp/legacy-framework',
    icon: APIneIcon,
    startDate: 'February 2015',
    endDate: 'December 2018',
    techs: [techs.php],
    description: [
      'An ambitious framework catering to the needs of two developers who wanted to create APIs and full fledged websites with routing, modules, and an MVC template. APIne was used on most of my projects until 2018, where I started to specialize in Javascript. While most of the framework was done by my friend @Youmy001, I did make improvements and features of my own.',
      'The project is now abandoned and even though a new version was being developed by @Youmy001 for a time, it is no longer maintained.'
    ]
  },
  {
    type: 'link',
    name: 'Small Name',
    url: 'https://smallname.binar.ca',
    code: 'https://gitlab.com/binarmorker/Small-Name',
    icon: SmallNameIcon,
    startDate: 'February 2016',
    endDate: 'January 2020',
    techs: [techs.php, techs.mysql, techs.bs],
    description: [
      'My take at a URL reducer, back when it was useful to have one handy. The project uses a pseudo random algorithm to generate hashes based on sequential numbers. The website is built using PHP, MySQL, and Bootstrap.',
      'It is currently retired.'
    ]
  },
  {
    type: 'link',
    name: 'Dinklebot',
    url: 'https://dinklebot.binar.ca',
    code: 'https://gitlab.com/binarmorker/Dinklebot',
    icon: DinklebotNetIcon,
    startDate: 'February 2015',
    endDate: 'September 2017',
    techs: [techs.php, techs.mysql, techs.bs, techs.linux],
    description: [
      'Destiny was always a game about showing off, and this website was made for it. It also fills the role of being a good game companion for completionists and weekly grinders.',
      'Dinklebot is made using PHP, MySQL, and Bootstrap.',
      'It is currently retired.'
    ]
  },
  {
    type: 'archived',
    name: 'Enterprise CRM',
    //url: '',
    //code: 'https://bitbucket.org/Youmy/indbags_forum',
    icon: EForumIcon,
    startDate: 'February 2015',
    endDate: 'January 2017',
    techs: [techs.php, techs.js, techs.mysql, techs.bs, techs.win],
    description: [
      'Developed for a client who needed to synchronize employee demands for products between countries.',
      'Initially using Drupal and other forum software, we managed to create what felt familiar, yet suited it for their needs.',
      'It is currently retired.'
    ]
  },
  {
    type: 'download',
    name: 'War of Raekidion',
    url: '/download/raekidion_0.7.5_windows.exe',
    code: 'https://bitbucket.org/Learz/raekidion',
    icon: WarOfRaekidionIcon,
    startDate: 'January 2014',
    endDate: 'August 2014',
    techs: [techs.eiffel, techs.sdl2],
    description: [
      'I made this game with my friend @Learz while in college, and we learned a lot about game making and contract-based programming during that time.',
      'It is programmed in Eiffel, a unique programming language that allowed us to wrap SDL 2 and use it to display sprites.',
      'The game is still downloadable, but the project is abandoned.'
    ]
  },
  {
    type: 'download',
    name: 'NES Blob',
    url: '/download/Unnamed_V0.6.0.nes',
    code: 'https://gitlab.com/binarmorker/NES_Blob',
    icon: NESBlobIcon,
    startDate: 'January 2014',
    endDate: 'August 2014',
    techs: [techs.asm],
    description: [
      'I made this game in 6502 Assembly for the NES while in college. In doing so, I unlocked a lot of knowledge about the compilation process, decompiling, using registers and interrupts, and calculating in binary and hexadecimal.',
      'The game should work on a NES or Famicom console, given you have a programmable cartridge. However, it is easier to play on an emulator.',
      'The game is still downloadable, but the project is abandoned.'
    ]
  },
]
