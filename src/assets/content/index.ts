import type { Component } from 'vue'

export type Website = {
  name: string,
  url?: string,
  code?: string,
  icon?: Component,
  startDate: string,
  endDate?: string,
  techs?: string[],
  oldTechs?: string[],
  type: 'download'|'link'|'archived'|'upcoming'|'hidden',
  description: string[]
}

export type Resume = {
  name: string,
  summary: string,
  achievements: string[],
  education: {
    title: string,
    location: string,
    startDate: string,
    endDate: string
  }[],
  realizations: {
    name: string,
    date: string,
    description: string
  }[],
  experience: {
    title: string,
    employer: string,
    startDate: string,
    endDate: string,
    achievements: string[]
  }[],
  criteria: string,
  skills: {
    name: string,
    proficiency: number
  }[],
}

export const techs = {
  js: 'Javascript',
  ts: 'Typescript',
  vue: 'Vue',
  fb: 'Firebase',
  twcss: 'TailwindCSS',
  pcss: 'PostCSS',
  djs: 'discord.js',
  exp: 'Express',
  elec: 'Electron',
  pup: 'Puppeteer',
  cf: 'Cloudflare Workers',
  mysql: 'MySQL',
  php: 'PHP',
  sqlite: 'SQLite',
  dotnet: '.NET',
  nuxt: 'Nuxt',
  vtfy: 'Vuetify',
  node: 'Node.js',
  ko: 'Knockout.js',
  bs: 'Bootstrap',
  sass: 'Sass',
  less: 'Less',
  eiffel: 'Eiffel',
  sdl2: 'SDL 2',
  asm: '6502 Assembly',
  java: 'Java',
  linux: 'Linux',
  win: 'Windows',
  docker: 'Docker',
  unity: 'Unity'
}

export const collaborators = new Map<string, string>([
  ['Utar94', 'https://www.francispion.ca'],
  ['FreddyAlabastro', 'https://twitter.com/FreddyAlabastro'],
  ['Wamien', 'https://www.twitch.tv/wamien'],
  ['Youmy001', 'https://gitlab.com/Youmy001'],
  ['Learz', 'https://marksfine.site']
])

export const resumePartial: Pick<Resume, 'skills'> = {
  skills: [
    {
      name: 'JS • TS • Vue.js • Node',
      proficiency: 100
    },
    {
      name: 'HTML • CSS • SASS',
      proficiency: 90
    },
    {
      name: 'C# • .NET',
      proficiency: 85
    },
    {
      name: 'DevOps • Win • Linux',
      proficiency: 80
    },
    {
      name: 'MySQL • SQL Server',
      proficiency: 75
    },
    {
      name: 'Azure • AWS • Google',
      proficiency: 65
    }
  ]
}
