import './style.css'
import messagesEn from './i18n/en.json'
import messagesFr from './i18n/fr.json'
import { createApp } from 'vue'
import { createI18n } from 'vue-i18n'
import App from './App.vue'

let language = 'en'
const userLanguage = window.navigator.language
const localLanguage = localStorage.getItem('locale')

if (userLanguage) {
  language = userLanguage.includes('fr') ? 'fr' : 'en'
}

if (localLanguage) {
  language = localLanguage
}

const i18n = createI18n({
  locale: language,
  fallbackLocale: 'en',
  messages: {
    en: messagesEn,
    fr: messagesFr
  }
})
const app = createApp(App)

app.use(i18n)
app.mount('#app')
